USE master
GO
-- Usuwanie bazy je�li istnieje
DROP DATABASE IF EXISTS DeliveryCompany
GO
-- Tworzenie bazy DeliveryComponany
CREATE DATABASE DeliveryCompany
GO

-- ustawienie bazy DeliveryCompnany
USE DeliveryCompany
GO

-----------------------------------------------------------------------------------
-- Tworzenie schematu Location
CREATE SCHEMA Location
Go

CREATE TABLE [Location].[Countries] (
 ID INT IDENTITY(1,1) PRIMARY KEY,
 UID UNIQUEIDENTIFIER DEFAULT NEWID(),
 Code NVARCHAR(20) UNIQUE NOT NULL,
 Name NVARCHAR(100) NOT NULL
)
GO

CREATE TABLE [Location].[Addresses] (
 ID INT IDENTITY(1,1) PRIMARY KEY,
 UID UNIQUEIDENTIFIER DEFAULT NEWID(),
 CountryId INT FOREIGN KEY REFERENCES [Location].[Countries](ID) NOT NULL,
 City NVARCHAR(400) NOT NULL,
 ZipCode NVARCHAR(20) NOT NULL,
 Street NVARCHAR(400),
 Number NVARCHAR(20) NOT NULL,
 FlatNumber NVARCHAR(20),
 Comments NVARCHAR(MAX)
)
GO

---------------------------------------------------------------------------------------
-- Tworzenie schematu HR
CREATE SCHEMA Hr
GO


CREATE TABLE [Hr].[Departments](
 ID INT IDENTITY(1,1) PRIMARY KEY,
 UID UNIQUEIDENTIFIER DEFAULT NEWID(),
 Code NVARCHAR(20) UNIQUE NOT NULL,
 Name NVARCHAR(100) NOT NULL,
 Comments NVARCHAR(MAX)
)


CREATE TABLE [Hr].[Positions](
 ID INT IDENTITY(1,1) PRIMARY KEY,
 UID UNIQUEIDENTIFIER DEFAULT NEWID(),
 Code NVARCHAR(20) UNIQUE NOT NULL,
 Name NVARCHAR(100) NOT NULL,
 Comments NVARCHAR(MAX)
)
GO

CREATE TABLE [Hr].[Employees]( 
 ID INT IDENTITY(1,1) PRIMARY KEY,
 UID UNIQUEIDENTIFIER DEFAULT NEWID(),
 FirtsName NVARCHAR(150) NOT NULL,
 SecondName NVARCHAR(150),
 LastName NVARCHAR(200) NOT NULL,
 AddressId INT FOREIGN KEY REFERENCES [Location].[Addresses](ID) NOT NULL,
 DepartmentId INT FOREIGN KEY REFERENCES [Hr].[Departments](ID) NOT NULL,
 PositionId INT FOREIGN KEY REFERENCES [Hr].[Positions](ID) NOT NULL,
 Salary DECIMAL(19,4) NOT NULL,
 ManagerId INT FOREIGN KEY REFERENCES [Hr].[Employees](ID),
 DateOfEmployment DATETIME2 NOT NULL,
 TerminationDate DATETIME2,
 MailAdreess NVARCHAR(200),
 Phone NVARCHAR(15),
 CreateDate DATETIME2,
 Active BIT DEFAULT 0 NOT NULL
)
GO

------------------------------------------------------------------------------------------------
-- Tworzenie schematu Logistic
CREATE SCHEMA Logistic
GO

CREATE TABLE [Logistic].[Statuses](
 ID INT IDENTITY(1,1) PRIMARY KEY,
 UID UNIQUEIDENTIFIER DEFAULT NEWID(),
 Code NVARCHAR(20) UNIQUE NOT NULL,
 Name NVARCHAR(100) NOT NULL,
 Comments NVARCHAR(MAX)
)

CREATE TABLE [Logistic].[Boxes](
 ID INT IDENTITY(1,1) PRIMARY KEY,
 UID UNIQUEIDENTIFIER DEFAULT NEWID(),
 StatusId INT FOREIGN KEY REFERENCES [Logistic].[Statuses](ID) NOT NULL,
 Weight DECIMAL(20,2) NOT NULL,
 Fragile BIT NOT NULL DEFAULT 0,
 Dengerous BIT NOT NULL DEFAULT 0,
 Height Decimal(20,2) NOT NULL, 
 Width Decimal(20,2) NOT NULL,
 Depth Decimal(20,2) NOT NULL,
 Comments NVARCHAR(MAX)
)
GO

-------------------------------------------------------------------------------------------------------
-- Tworzenie schematu Orders
CREATE SCHEMA Orders
GO

CREATE TABLE [Orders].[Clients](
 ID INT IDENTITY(1,1) PRIMARY KEY,
 UID UNIQUEIDENTIFIER DEFAULT NEWID(),
 FirtsName NVARCHAR(150) NOT NULL,
 SecondName NVARCHAR(150),
 LastName NVARCHAR(200) NOT NULL,
 AddressId INT FOREIGN KEY REFERENCES [Location].[Addresses](ID) NOT NULL,
 MailAdreess NVARCHAR(200),
 Phone NVARCHAR(15),
 CreateDate DATETIME2,
 UpdateDate DATETIME2,
 Active BIT DEFAULT 0 NOT NULL
)
GO

CREATE TABLE [Orders].[Trips](
 ID INT IDENTITY(1,1) PRIMARY KEY,
 UID UNIQUEIDENTIFIER DEFAULT NEWID(),
 BoxId INT FOREIGN KEY REFERENCES [Logistic].[Boxes](ID) NOT NULL,
 StartClient INT FOREIGN KEY REFERENCES [Orders].[Clients](ID) NOT NULL,
 EndClient INT FOREIGN KEY REFERENCES [Orders].[Clients](ID) NOT NULL,
 OperatorId INT FOREIGN KEY REFERENCES [Hr].[Employees](ID) NOT NULL,
 TotalCost DECIMAL(19,4),
 CreateDate DATETIME2,
 DeliveredDate DATETIME2,
 Active BIT DEFAULT 0 NOT NULL
)
GO






