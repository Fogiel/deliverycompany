$ProjectName = 'DeliveryCompany'
$DataBaseName = "DeliveryCompany"
$ConnectionString = "Server=.\SQLEXPRESS;Database=$DataBaseName;Integrated Security=True"
$DataBaseContext = "$DataBaseName" + "Context"

dotnet new sln --name $ProjectName
dotnet new mvc --name $ProjectName --output $ProjectName
dotnet sln add ".\$ProjectName\$ProjectName.csproj"
dotnet restore
dotnet build "$ProjectName.sln"

dotnet tool install -g dotnet-ef
dotnet tool install -g dotnet-aspnet-codegenerator

SET-LOCATION $ProjectName
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet ef dbcontext scaffold $ConnectionString Microsoft.EntityFrameworkCore.SqlServer -o Models --data-annotations -f

SET-LOCATION Models
$models = (Get-ChildItem).BaseName 
SET-LOCATION ..

foreach ($model in $models)
{
    if ($model -eq $DataBaseContext -or $model -eq "ErrorViewModel")
    {
        continue
    }
    else 
    {
        dotnet-aspnet-codegenerator controller -m $model -dc $DataBaseContext -outDir "Controllers" -name "${model}Controller" -udl -scripts 
    }
}

$appsettingsPath = "appsettings.json"
$content = Get-Content $appsettingsPath -Raw
$jobj = ConvertFrom-Json -InputObject $content

$context = @"
    {
        "${DataBaseContext}": "Server=.\\SQLEXPRESS;Database=${DataBaseName};Integrated Security=True"
    }
"@

$jobj | add-member -Name ConnectionStrings -Value (Convertfrom-Json $context) -MemberType NoteProperty
Set-Content -Path $appsettingsPath -Value (convertTo-Json $jobj)

$StartupPath = "Startup.cs"
$startupContent = Get-Content -path $StartupPath
$startupContent[4] += "`r`n"
$startupContent[4] += "using Microsoft.EntityFrameworkCore; `r`n"
$startupContent[4] += "using ${ProjectName}.Models; `r`n"

$lineNumber = $startupContent | Select-String -Pattern "services.AddControllersWithViews()" | Select-Object LineNumber
$startupContent[$lineNumber.LineNumber-1] += "`r`n"
$startupContent[$lineNumber.LineNumber-1] +="`t`t`tservices.AddDbContext<$DataBaseContext>(options => options.UseSqlServer(Configuration.GetConnectionString(""$DataBaseContext"")));"

$startupContent | Set-Content $StartupPath

$LayoutPath = "Views/Shared/_Layout.cshtml"
$LayoutConent = Get-Content -path $LayoutPath
$lineNumberObject = $LayoutConent | Select-String -Pattern "<a class=""nav-link text-dark"" asp-area="""" asp-controller=""Home"" asp-action=""Privacy"">Privacy</a>" | Select-Object LineNumber
$lineNumber = $lineNumberObject.LineNumber;

foreach ($model in $models)
{
    if ($model -eq $DataBaseContext -or $model -eq "ErrorViewModel")
    {
        continue
    }
    else 
    {
        $LayoutConent = Get-Content -path $LayoutPath
        $LayoutConent[$lineNumber] += "`r`n";
        $LayoutConent[$lineNumber++] += "`t`t`t`t`t`t<li class=""nav-item"">";
        $LayoutConent | Set-Content $LayoutPath
        $LayoutConent = Get-Content -path $LayoutPath
        $LayoutConent[$lineNumber] += "`r`n";
        $LayoutConent[$lineNumber++] += "`t`t`t`t`t`t`t<a class=""nav-link text-dark"" asp-area="""" asp-controller=""${model}"" asp-action=""Index"">${model}</a>";
        $LayoutConent | Set-Content $LayoutPath
        $LayoutConent = Get-Content -path $LayoutPath
        $LayoutConent[$lineNumber] += "`r`n";
        $LayoutConent[$lineNumber++] += "`t`t`t`t`t`t</li>";
        $LayoutConent | Set-Content $LayoutPath
    }
}

