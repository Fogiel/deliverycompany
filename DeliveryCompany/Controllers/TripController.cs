using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DeliveryCompany.Models;
using Microsoft.AspNetCore.Authorization;

namespace DeliveryCompany.Controllers
{
    [Authorize]
    public class TripController : Controller
    {
        private readonly DeliveryCompanyContext _context;

        public TripController(DeliveryCompanyContext context)
        {
            _context = context;
        }

        // GET: Trip
        public async Task<IActionResult> Index()
        {
            var deliveryCompanyContext = _context.Trips.Include(t => t.Box).Include(t => t.EndClientNavigation).Include(t => t.Operator).Include(t => t.StartClientNavigation);
            return View(await deliveryCompanyContext.ToListAsync());
        }

        // GET: Trip/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trip = await _context.Trips
                .Include(t => t.Box)
                .Include(t => t.EndClientNavigation)
                .Include(t => t.Operator)
                .Include(t => t.StartClientNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trip == null)
            {
                return NotFound();
            }

            return View(trip);
        }

        // GET: Trip/Create
        public IActionResult Create()
        {
            ViewData["BoxId"] = new SelectList(_context.Boxes, "Id", "Id");
            ViewData["EndClient"] = new SelectList(_context.Clients, "Id", "FirtsName");
            ViewData["OperatorId"] = new SelectList(_context.Employees, "Id", "FirtsName");
            ViewData["StartClient"] = new SelectList(_context.Clients, "Id", "FirtsName");
            return View();
        }

        // POST: Trip/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Uid,BoxId,StartClient,EndClient,OperatorId,TotalCost,CreateDate,DeliveredDate,Active")] Trip trip)
        {
            if (ModelState.IsValid)
            {
                _context.Add(trip);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BoxId"] = new SelectList(_context.Boxes, "Id", "Id", trip.BoxId);
            ViewData["EndClient"] = new SelectList(_context.Clients, "Id", "FirtsName", trip.EndClient);
            ViewData["OperatorId"] = new SelectList(_context.Employees, "Id", "FirtsName", trip.OperatorId);
            ViewData["StartClient"] = new SelectList(_context.Clients, "Id", "FirtsName", trip.StartClient);
            return View(trip);
        }

        // GET: Trip/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trip = await _context.Trips.FindAsync(id);
            if (trip == null)
            {
                return NotFound();
            }
            ViewData["BoxId"] = new SelectList(_context.Boxes, "Id", "Id", trip.BoxId);
            ViewData["EndClient"] = new SelectList(_context.Clients, "Id", "FirtsName", trip.EndClient);
            ViewData["OperatorId"] = new SelectList(_context.Employees, "Id", "FirtsName", trip.OperatorId);
            ViewData["StartClient"] = new SelectList(_context.Clients, "Id", "FirtsName", trip.StartClient);
            return View(trip);
        }

        // POST: Trip/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Uid,BoxId,StartClient,EndClient,OperatorId,TotalCost,CreateDate,DeliveredDate,Active")] Trip trip)
        {
            if (id != trip.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(trip);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TripExists(trip.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BoxId"] = new SelectList(_context.Boxes, "Id", "Id", trip.BoxId);
            ViewData["EndClient"] = new SelectList(_context.Clients, "Id", "FirtsName", trip.EndClient);
            ViewData["OperatorId"] = new SelectList(_context.Employees, "Id", "FirtsName", trip.OperatorId);
            ViewData["StartClient"] = new SelectList(_context.Clients, "Id", "FirtsName", trip.StartClient);
            return View(trip);
        }

        // GET: Trip/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trip = await _context.Trips
                .Include(t => t.Box)
                .Include(t => t.EndClientNavigation)
                .Include(t => t.Operator)
                .Include(t => t.StartClientNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trip == null)
            {
                return NotFound();
            }

            return View(trip);
        }

        // POST: Trip/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var trip = await _context.Trips.FindAsync(id);
            _context.Trips.Remove(trip);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TripExists(int id)
        {
            return _context.Trips.Any(e => e.Id == id);
        }
    }
}
