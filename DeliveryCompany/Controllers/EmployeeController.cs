using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DeliveryCompany.Models;
using Microsoft.AspNetCore.Authorization;

namespace DeliveryCompany.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {
        private readonly DeliveryCompanyContext _context;

        public EmployeeController(DeliveryCompanyContext context)
        {
            _context = context;
        }

        // GET: Employee
        public async Task<IActionResult> Index()
        {
            var deliveryCompanyContext = _context.Employees.Include(e => e.Address).Include(e => e.Department).Include(e => e.Manager).Include(e => e.Position);
            return View(await deliveryCompanyContext.ToListAsync());
        }

        // GET: Employee/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .Include(e => e.Address)
                .Include(e => e.Department)
                .Include(e => e.Manager)
                .Include(e => e.Position)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // GET: Employee/Create
        public IActionResult Create()
        {
            ViewData["AddressId"] = new SelectList(_context.Addresses, "Id", "City");
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Code");
            ViewData["ManagerId"] = new SelectList(_context.Employees, "Id", "FirtsName");
            ViewData["PositionId"] = new SelectList(_context.Positions, "Id", "Code");
            return View();
        }

        // POST: Employee/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Uid,FirtsName,SecondName,LastName,AddressId,DepartmentId,PositionId,Salary,ManagerId,DateOfEmployment,TerminationDate,MailAdreess,Phone,CreateDate,Active")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                _context.Add(employee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AddressId"] = new SelectList(_context.Addresses, "Id", "City", employee.AddressId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Code", employee.DepartmentId);
            ViewData["ManagerId"] = new SelectList(_context.Employees, "Id", "FirtsName", employee.ManagerId);
            ViewData["PositionId"] = new SelectList(_context.Positions, "Id", "Code", employee.PositionId);
            return View(employee);
        }

        // GET: Employee/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            ViewData["AddressId"] = new SelectList(_context.Addresses, "Id", "City", employee.AddressId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Code", employee.DepartmentId);
            ViewData["ManagerId"] = new SelectList(_context.Employees, "Id", "FirtsName", employee.ManagerId);
            ViewData["PositionId"] = new SelectList(_context.Positions, "Id", "Code", employee.PositionId);
            return View(employee);
        }

        // POST: Employee/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Uid,FirtsName,SecondName,LastName,AddressId,DepartmentId,PositionId,Salary,ManagerId,DateOfEmployment,TerminationDate,MailAdreess,Phone,CreateDate,Active")] Employee employee)
        {
            if (id != employee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(employee.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AddressId"] = new SelectList(_context.Addresses, "Id", "City", employee.AddressId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Code", employee.DepartmentId);
            ViewData["ManagerId"] = new SelectList(_context.Employees, "Id", "FirtsName", employee.ManagerId);
            ViewData["PositionId"] = new SelectList(_context.Positions, "Id", "Code", employee.PositionId);
            return View(employee);
        }

        // GET: Employee/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .Include(e => e.Address)
                .Include(e => e.Department)
                .Include(e => e.Manager)
                .Include(e => e.Position)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employee = await _context.Employees.FindAsync(id);
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }
    }
}
