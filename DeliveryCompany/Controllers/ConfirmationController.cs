﻿using DeliveryCompany.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace DeliveryCompany.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfirmationController : ControllerBase
    {
        private readonly DeliveryCompanyContext _context;

        public ConfirmationController(DeliveryCompanyContext context)
        {
            _context = context;
        }

        [HttpPut]
        public async Task<IActionResult> Confirm(ConfirmationModel confirmationModel)
        {
            var trip = await _context.Trips
                   .Include(b => b.Box)
                   .SingleOrDefaultAsync(x => x.Id == confirmationModel.TripId && x.OperatorId == confirmationModel.OperatorId);
            if (trip == null)
            {
                return BadRequest("Trip Not Found");
            }

            if (trip.DeliveredDate.HasValue)
            {
                return BadRequest($"Trip end at {trip.DeliveredDate.Value}");
            }

            trip.DeliveredDate = DateTime.UtcNow;
            trip.Box.Status = await _context.Statuses.SingleOrDefaultAsync(x => x.Code == "FIN");
            
            _context.Update(trip);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
