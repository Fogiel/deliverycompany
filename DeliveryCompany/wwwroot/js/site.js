﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


function onConfirmationSubmit() {
    const data = {
        tripId: $("#tripId").val(),
        operatorId: $("#operatorId").val()
    }
    $.ajax({
        url: 'https://localhost:44300/api/Confirmation/',
        type: 'PUT',
        data: JSON.stringify(data),
        contentType: "application/json"
    }).done(function () {
        alert('Trip Updated');
    }).fail(function (msg) {
        alert(msg.responseText);
    }).always(function () {
        $('#closeButton').click();
    });
}

function toogleMenu(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
}
