﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DeliveryCompany.ViewsComponents
{
    public class UserComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            this.TempData["User"] = HttpContext.Session.GetString("User");
            
            return View();
        }
    }
}
