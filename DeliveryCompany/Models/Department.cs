﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DeliveryCompany.Models
{
    [Table("Departments", Schema = "Hr")]
    [Index(nameof(Code), Name = "UQ__Departme__A25C5AA71F3E9498", IsUnique = true)]
    public partial class Department
    {
        public Department()
        {
            Employees = new HashSet<Employee>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("UID")]
        public Guid? Uid { get; set; }
        [Required]
        [StringLength(20)]
        public string Code { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public string Comments { get; set; }

        [InverseProperty(nameof(Employee.Department))]
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
