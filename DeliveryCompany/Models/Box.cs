﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DeliveryCompany.Models
{
    [Table("Boxes", Schema = "Logistic")]
    public partial class Box
    {
        public Box()
        {
            Trips = new HashSet<Trip>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("UID")]
        public Guid? Uid { get; set; }
        public int StatusId { get; set; }
        [Column(TypeName = "decimal(20, 2)")]
        public decimal Weight { get; set; }
        public bool Fragile { get; set; }
        public bool Dengerous { get; set; }
        [Column(TypeName = "decimal(20, 2)")]
        public decimal Height { get; set; }
        [Column(TypeName = "decimal(20, 2)")]
        public decimal Width { get; set; }
        [Column(TypeName = "decimal(20, 2)")]
        public decimal Depth { get; set; }
        public string Comments { get; set; }

        [ForeignKey(nameof(StatusId))]
        [InverseProperty("Boxes")]
        public virtual Status Status { get; set; }
        [InverseProperty(nameof(Trip.Box))]
        public virtual ICollection<Trip> Trips { get; set; }
    }
}
