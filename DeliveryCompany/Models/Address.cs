﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DeliveryCompany.Models
{
    [Table("Addresses", Schema = "Location")]
    public partial class Address
    {
        public Address()
        {
            Clients = new HashSet<Client>();
            Employees = new HashSet<Employee>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("UID")]
        public Guid? Uid { get; set; }
        public int CountryId { get; set; }
        [Required]
        [StringLength(400)]
        public string City { get; set; }
        [Required]
        [StringLength(20)]
        public string ZipCode { get; set; }
        [StringLength(400)]
        public string Street { get; set; }
        [Required]
        [StringLength(20)]
        public string Number { get; set; }
        [StringLength(20)]
        public string FlatNumber { get; set; }
        public string Comments { get; set; }

        [ForeignKey(nameof(CountryId))]
        [InverseProperty("Addresses")]
        public virtual Country Country { get; set; }
        [InverseProperty(nameof(Client.Address))]
        public virtual ICollection<Client> Clients { get; set; }
        [InverseProperty(nameof(Employee.Address))]
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
