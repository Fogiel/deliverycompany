﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DeliveryCompany.Models
{
    [Table("Employees", Schema = "Hr")]
    public partial class Employee
    {
        public Employee()
        {
            InverseManager = new HashSet<Employee>();
            Trips = new HashSet<Trip>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("UID")]
        public Guid? Uid { get; set; }
        [Required]
        [StringLength(150)]
        public string FirtsName { get; set; }
        [StringLength(150)]
        public string SecondName { get; set; }
        [Required]
        [StringLength(200)]
        public string LastName { get; set; }
        public int AddressId { get; set; }
        public int DepartmentId { get; set; }
        public int PositionId { get; set; }
        [Column(TypeName = "decimal(19, 4)")]
        public decimal Salary { get; set; }
        public int? ManagerId { get; set; }
        public DateTime DateOfEmployment { get; set; }
        public DateTime? TerminationDate { get; set; }
        [StringLength(200)]
        public string MailAdreess { get; set; }
        [StringLength(15)]
        public string Phone { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool Active { get; set; }

        [ForeignKey(nameof(AddressId))]
        [InverseProperty("Employees")]
        public virtual Address Address { get; set; }
        [ForeignKey(nameof(DepartmentId))]
        [InverseProperty("Employees")]
        public virtual Department Department { get; set; }
        [ForeignKey(nameof(ManagerId))]
        [InverseProperty(nameof(Employee.InverseManager))]
        public virtual Employee Manager { get; set; }
        [ForeignKey(nameof(PositionId))]
        [InverseProperty("Employees")]
        public virtual Position Position { get; set; }
        [InverseProperty(nameof(Employee.Manager))]
        public virtual ICollection<Employee> InverseManager { get; set; }
        [InverseProperty(nameof(Trip.Operator))]
        public virtual ICollection<Trip> Trips { get; set; }
    }
}
