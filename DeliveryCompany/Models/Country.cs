﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DeliveryCompany.Models
{
    [Table("Countries", Schema = "Location")]
    [Index(nameof(Code), Name = "UQ__Countrie__A25C5AA7DE13EB00", IsUnique = true)]
    public partial class Country
    {
        public Country()
        {
            Addresses = new HashSet<Address>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("UID")]
        public Guid? Uid { get; set; }
        [Required]
        [StringLength(20)]
        public string Code { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [InverseProperty(nameof(Address.Country))]
        public virtual ICollection<Address> Addresses { get; set; }
    }
}
