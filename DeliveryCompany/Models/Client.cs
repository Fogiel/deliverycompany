﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DeliveryCompany.Models
{
    [Table("Clients", Schema = "Orders")]
    public partial class Client
    {
        public Client()
        {
            TripEndClientNavigations = new HashSet<Trip>();
            TripStartClientNavigations = new HashSet<Trip>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("UID")]
        public Guid? Uid { get; set; }
        [Required]
        [StringLength(150)]
        public string FirtsName { get; set; }
        [StringLength(150)]
        public string SecondName { get; set; }
        [Required]
        [StringLength(200)]
        public string LastName { get; set; }
        public int AddressId { get; set; }
        [StringLength(200)]
        public string MailAdreess { get; set; }
        [StringLength(15)]
        public string Phone { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool Active { get; set; }

        [ForeignKey(nameof(AddressId))]
        [InverseProperty("Clients")]
        public virtual Address Address { get; set; }
        [InverseProperty(nameof(Trip.EndClientNavigation))]
        public virtual ICollection<Trip> TripEndClientNavigations { get; set; }
        [InverseProperty(nameof(Trip.StartClientNavigation))]
        public virtual ICollection<Trip> TripStartClientNavigations { get; set; }
    }
}
