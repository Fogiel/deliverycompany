﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DeliveryCompany.Models
{
    [Table("Trips", Schema = "Orders")]
    public partial class Trip
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("UID")]
        public Guid? Uid { get; set; }
        public int BoxId { get; set; }
        public int StartClient { get; set; }
        public int EndClient { get; set; }
        public int OperatorId { get; set; }
        [Column(TypeName = "decimal(19, 4)")]
        public decimal? TotalCost { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? DeliveredDate { get; set; }
        public bool Active { get; set; }

        [ForeignKey(nameof(BoxId))]
        [InverseProperty("Trips")]
        public virtual Box Box { get; set; }
        [ForeignKey(nameof(EndClient))]
        [InverseProperty(nameof(Client.TripEndClientNavigations))]
        public virtual Client EndClientNavigation { get; set; }
        [ForeignKey(nameof(OperatorId))]
        [InverseProperty(nameof(Employee.Trips))]
        public virtual Employee Operator { get; set; }
        [ForeignKey(nameof(StartClient))]
        [InverseProperty(nameof(Client.TripStartClientNavigations))]
        public virtual Client StartClientNavigation { get; set; }
    }
}
