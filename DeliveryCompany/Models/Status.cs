﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DeliveryCompany.Models
{
    [Table("Statuses", Schema = "Logistic")]
    [Index(nameof(Code), Name = "UQ__Statuses__A25C5AA77F1CF744", IsUnique = true)]
    public partial class Status
    {
        public Status()
        {
            Boxes = new HashSet<Box>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("UID")]
        public Guid? Uid { get; set; }
        [Required]
        [StringLength(20)]
        public string Code { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public string Comments { get; set; }

        [InverseProperty(nameof(Box.Status))]
        public virtual ICollection<Box> Boxes { get; set; }
    }
}
