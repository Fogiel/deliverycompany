﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace DeliveryCompany.Models
{
    public partial class DeliveryCompanyContext : DbContext
    {
        private readonly IConfiguration configuration;
        public DeliveryCompanyContext()
        {
        }

        public DeliveryCompanyContext(DbContextOptions<DeliveryCompanyContext> options, IConfiguration configuration)
            : base(options)
        {
            this.configuration = configuration;
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Box> Boxes { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }
        public virtual DbSet<Trip> Trips { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(this.configuration.GetConnectionString("DeliveryCompanyContext"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Polish_CI_AS");

            modelBuilder.Entity<Address>(entity =>
            {
                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Addresses)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Addresses__Count__29572725");
            });

            modelBuilder.Entity<Box>(entity =>
            {
                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Boxes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Boxes__StatusId__403A8C7D");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Clients)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Clients__Address__45F365D3");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employees__Addre__34C8D9D1");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employees__Depar__35BCFE0A");

                entity.HasOne(d => d.Manager)
                    .WithMany(p => p.InverseManager)
                    .HasForeignKey(d => d.ManagerId)
                    .HasConstraintName("FK__Employees__Manag__37A5467C");

                entity.HasOne(d => d.Position)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.PositionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employees__Posit__36B12243");
            });

            modelBuilder.Entity<Position>(entity =>
            {
                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Trip>(entity =>
            {
                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Box)
                    .WithMany(p => p.Trips)
                    .HasForeignKey(d => d.BoxId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Trips__BoxId__4AB81AF0");

                entity.HasOne(d => d.EndClientNavigation)
                    .WithMany(p => p.TripEndClientNavigations)
                    .HasForeignKey(d => d.EndClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Trips__EndClient__4CA06362");

                entity.HasOne(d => d.Operator)
                    .WithMany(p => p.Trips)
                    .HasForeignKey(d => d.OperatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Trips__OperatorI__4D94879B");

                entity.HasOne(d => d.StartClientNavigation)
                    .WithMany(p => p.TripStartClientNavigations)
                    .HasForeignKey(d => d.StartClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Trips__StartClie__4BAC3F29");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
