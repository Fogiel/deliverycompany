﻿
namespace DeliveryCompany.Models
{
    public class ConfirmationModel
    {
        public int TripId { get; set; }
        public int OperatorId { get; set; }
    }
}
