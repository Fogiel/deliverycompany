﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfirmationApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button1.Enabled = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox1.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                textBox1.Text = textBox1.Text.Remove(textBox1.Text.Length - 1);
                button1.Enabled = false;
                return;
            }
            button1.Enabled = !string.IsNullOrEmpty(textBox1.Text) && !string.IsNullOrEmpty(textBox2.Text);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox2.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                textBox2.Text = textBox2.Text.Remove(textBox2.Text.Length - 1);
                button1.Enabled = false;
                return;
            }
            button1.Enabled = !string.IsNullOrEmpty(textBox1.Text) && !string.IsNullOrEmpty(textBox2.Text);
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            using(var httpClinet = new HttpClient())
            {
                var model = new ConfirmationModel
                {
                    OperatorId = int.Parse(textBox1.Text),
                    TripId = int.Parse(textBox1.Text),
                };
                var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var url = $"https://localhost:44300/api/Confirmation/";
                var response = await httpClinet.PutAsync(url, content);
                if(response.IsSuccessStatusCode)
                {
                    MessageBox.Show("Udało się:)");
                }
                else
                {
                    MessageBox.Show(await response.Content.ReadAsStringAsync());
                }

                button1.Enabled = true;
            }
        }

        public class ConfirmationModel
        {
            public int TripId { get; set; }
            public int OperatorId { get; set; }
        }
    }
}
